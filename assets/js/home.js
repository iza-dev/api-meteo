const weatherIcons ={
    "Rain": "wi wi-day-rain",
    "Clouds": "wi wi-day-cloudy",
    "Clear": "wi wi-day-sunny",
    "Snow": "wi wi-day-snow",
    "Mist": "wi wi-day-fog",
    "Drizzle": "wi wi-day-sleet"
}

/*** fonction asynchrone pour une execution sequentielle ***/
 async function userInfo(){

    /*** récupération de l'ip ***/
    const ip = await fetch('https://api.ipify.org?format=json')
        .then(resultat => resultat.json())
        .then(json => (json.ip))
        .catch(error => 'An error occurred while loading the ip')

    /*** récupération du nom de la ville avec l'ip ***/
    const city = await fetch('http://api.ipstack.com/'+ip+'?access_key=464887f130fa271ffecf4bcf24bc2ccb')
        .then(resultat => resultat.json())
        .then(json => (json.city))
        .catch(error => 'An error occurred while loading the city')

    /*** récupération du nom de la temperature avec le nom de la ville ***/
    const weather = await fetch('http://api.openweathermap.org/data/2.5/weather?q='+city+'&lang=fr&APPID=a0ea1eb648fddf8fd5c26ebb7b0778c2')
        .then(resultat => resultat.json())
        .then(json => json)
        .catch(error => 'An error occurred while loading the city')
    displayUserInfo(city, weather);
 };

/*** afficher les data météo ***/
function displayUserInfo(city, weather){
    const citySection = document.getElementById('city');
    citySection.textContent = 'Votre ville est '+ city;
    const temp = document.getElementById('temperature');
    temp.textContent = Math.floor(weather.main.temp-273.15)+'° Deg' ;
    const iconWeather= document.getElementById('icon');
    iconWeather.className = weatherIcons[weather.weather[0].main];
    const descriptionWeather= document.getElementById('description');
    descriptionWeather.textContent =weather.weather[0].main;
};

userInfo();
