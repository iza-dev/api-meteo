const weatherIcons ={
    "Rain": "wi wi-day-rain",
    "Clouds": "wi wi-day-cloudy",
    "Clear": "wi wi-day-sunny",
    "Snow": "wi wi-day-snow",
    "Mist": "wi wi-day-fog",
    "Drizzle": "wi wi-day-sleet"
}

//écoute du chargement de la page
window.addEventListener('load', () =>{
    const localisation  = document.querySelector('#city');
    const temperature = document.querySelector('#temperature');
    const description = document.querySelector('#description');
    const icon = document.getElementById('icon');
    const root = document.getElementById('cityChoose');
    const startingMeteo = root.dataset.city;
    
    const data = JSON.parse(startingMeteo);
    //const iconurl = "http://openweathermap.org/img/w/" + data.weather[0].icon + ".png";

    //calcul pour covertir le kelvin en degré celsuis
    temperature.textContent = Math.floor(data.main.temp-273.15)+'° Deg' ;
    description.textContent = data.weather[0].main;
    localisation.textContent = data.name;
    icon.className = weatherIcons[data.weather[0].main];
})

