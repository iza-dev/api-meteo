<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="chooseMeteo")
     */
    public function setMeteo()
    {
        return $this->render('main/home.html.twig');

    }

    /**
     * @Route("/meteo", name="getMeteo")
     */
    public function getMeteo(Request $request, SerializerInterface $serializer)
    {
        $dataMeteo = null;
        $city = $request->query->get('city');
       // dump($city);

        if ($city != null) {
            $dataMeteo = file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$city.'&lang=fr&APPID=a0ea1eb648fddf8fd5c26ebb7b0778c2');
        }
            //retourne la template avec le tableau de données mééto de la ville choisie
            return $this->render('main/index.html.twig',['city'=> $dataMeteo]);
    }
}
