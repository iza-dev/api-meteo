<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

//pas besoin d'annptaions pour la persister en bdd.
class Meteo{

    protected $name;
    protected $main;

    public function __construct()
    {
        $this->main = new ArrayCollection();
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of main
     */ 
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set the value of main
     *
     * @return  self
     */ 
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }
}